# MLops homework: Feast

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

Based upon a Snakemake howework. A reproducible Snakemake pipeline. See docs/dag.md for the graph and description.

Disclaimer: `on_demand_feature_view` is currently broken with recent pandas/arrow. The upstream has no plans to fix that ATM (nice '''production readiness''' 🤡🤡🤡). I'm not being paid to work it around.

Instead, feature engineering is done externally. Feast provides both training and testing features on `historical features` basis.


The report is also deployed at at https://homework-11-mlops3421713-b2991077bbd8fe9cf32ad97914bfb618c61796.gitlab.io/

## Running:

Expects a `$CI_PROJECT_DIR` (upper level) or `$FEAST_DIR` (directory containing `feature_store.yaml`) to be defined!

E.g.: `export CI_PROJECT_DIR=/home/user/sources/homework_11`

Create environment:

`conda env create -f feast.yaml`

Initialize feast if needed:

`cd feast_repos/feature_repo`

`feast apply`

`cd ../..`

Run the pipeline:

`conda run -n dev snakemake --cores all`
