FROM continuumio/miniconda3

WORKDIR /app

# Creating environment
COPY feast.yaml .
RUN conda env create -f feast.yaml

# Copying project settings
COPY pyproject.toml .
