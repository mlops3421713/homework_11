from sklearn.preprocessing import StandardScaler, PowerTransformer
from feast_repos.feature_repo.retrieval import get_train, get_test

scalers = {
    "standard": StandardScaler(),
    "boxcox": PowerTransformer(),
}

df_train = get_train()
df_test = get_test()

scaler = scalers.get(snakemake.params.scaler, StandardScaler())
df_train[df_train.columns[:-1]] = scaler.fit_transform(
    df_train[df_train.columns[:-1]]
)
df_test[df_test.columns[:-1]] = scaler.transform(df_test[df_test.columns[:-1]])

df_train.to_csv(snakemake.output.train, index=False)
df_test.to_csv(snakemake.output.test, index=False)
